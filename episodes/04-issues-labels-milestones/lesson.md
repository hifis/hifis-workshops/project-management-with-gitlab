<!---
SPDX-FileCopyrightText: 2020 Helmholtz-Zentrum Dresden - Rossendorf (HZDR) <hifis-info@hzdr.de>

SPDX-License-Identifier: CC-BY-4.0
-->

# Issues, Labels and Milestones
> **Estimated time:** 30 minutes
>
> ## Learning Goals
> * What are issues, labels, boards and milestones?
> * How do these interact with each other?
>
> **Note:** When adding files in this episode, we do not concern ourself with branches and merge requests yet.
> It is okay to just directly put the files on _master_.

---

## [Issues]
* Issues are a conversation-style medium for outlining, planning and managing tasks and processes
* They form the entry point for common use-cases like
  * Bug reports 
  * Feature requests
  * Discussions and decision making
* Issues can be assigned to a person who is responsible for handling the issue
* A real world example: https://gitlab.com/gitlab-org/gitlab/-/issues

### How to Write Good Issues
Writing good issues is a valuable soft-skill for teamwork.
A good issue draws attention, has high information density and provides all details to get started with solving it.

* If available, use the appropriate issue template
* Be brief and precise
* Use the markdown formatting
  * GitLab supports [Mermaid] for simple diagrams 
* Provide all required details 
  * Prefer to attach large files like logs instead of including them directly

### When and How to Close Issues
* Issues can lead to _merge requests_
  * When this merge request gets closed or merged, the associated issue automatically will be closed
* Issues can also be closed directly if no merge request is needed
  * When closing an issue without a merge request, give a reason and/or summary as last comment
* It is recommended to regularly (e.g. monthly) schedule an issue-housekeeping
  * Check if issues are still relevant
  * Add updates to these issues if available
  * Close issues that are no longer valid
    * If the issue has been solved, add a link to the commit or merge request that solved it

> ### Do Together
> * Create a new issue
> * Assign the issue to someone
> * Comment on the issue
> * Close the issue

## [Labels]
* Labels are a tool for categorization
* They can be attached to _issues_ or _merge requests_
* Available either for a single project (_project label_) or all projects within a group (_group label_) 
* Labels have a name, an optional description and a color
  * Colors can be chosen from a list or by a [RGB Hex code]
  * **Note:** The intuitive meaning of colors can vary between cultures
* Labels can be prioritized and manually given a priority
* **Do** agree on a label policy and document it in the contribution guide

### [Scoped Labels]
```
Full support requires GitLab premium or higher tiers
```

* Used by naming the label according to the pattern `scope::label`
  * Example: `Priority::High`
* Allows to sort labels into categories

### Example Use-Cases for Labels
* Workflow-oriented
  * `ToDo`, `Planning`, `In Progress`, `Review`
* Type-oriented
  * `Bug`, `Fix`, `Feature`, `Maintenance`, `Chore`, `Documentation`, `Discussion` 
* Priority-oriented
  * `Critical`, `High`, `Medium`, `Low`
* Affected subsystems
* Expected difficulty
* Common other labels
  * `Help Wanted`, `Good First Issue`

> ### Do Together
> * Create a set of labels for the project including 
> `Priority::High`, `Priority::Normal`, `Priority::Low` and `Progress::ToDo`
> * Create a new issue `Update Contribution Guide with Label Policy`
> * Add some labels to the issue, including `Progress::ToDo`
> * Update the contribution guide accordingly
>   * We have not talked about merge requests yet, do it via WebIDE on master
> * Remove the `Progress::ToDo` labels on the issue
> * Close the issue

## [Boards]
_Issue boards_ are a helpful tool for project managers to quickly gain an overview over the current progress of tasks.

* Inspired by _Kanban_-Boards
* A board is built from _lists_ and each list contains all open issues with a specific label
* Moving issues around on the board will change the labels accordingly
* New issues can also be created directly from the boards and will automatically have the appropriate label

> ## Do Together
> * Set up a new board for priorities
> * Create a list for each priority label
> * Create a new issue from the board view

## [Milestones]
* Milestones represent intermediate development goals, e.g. 
  * A complex feature, 
  * An iteration of an experiment or 
  * A new software release
* Issues can be gathered into _Milestones_
* A Milestone is considered complete if all associated issues are closed 

> ## Do Together
> * Create a new issue `Learn to set up Milestones`
> * Create a new milestone
> * Assign the issue to the milestone
> * Close the issue
>   * Note that the milestone now is closed as well

## Other Notable Features
* There are also so-called _[Epics]_ to organize issues and milestones on an even higher level
  * Currently only available in _GitLab premium_ and higher tiers
  * Useful for managing multiple interacting projects

> ## Wrap-Up
> * Issues are the core concept for discussions within the scope of the project
> * They can be enhanced by labels, boards and milestones

[Epics]: https://docs.gitlab.com/ee/user/group/epics/
[RGB Hex code]: https://www.color-hex.com/
[Scoped Labels]: https://docs.gitlab.com/ee/user/project/labels.html#scoped-labels-premium
[Issues]: https://docs.gitlab.com/ee/user/project/issues/managing_issues.html
[Labels]: https://docs.gitlab.com/ee/user/project/labels.html
[Boards]: https://docs.gitlab.com/ee/user/project/issue_board.html
[Milestones]: https://docs.gitlab.com/ee/user/project/milestones/
[Mermaid]: https://mermaid-js.github.io/mermaid/#/
